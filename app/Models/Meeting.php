<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $fillable = ['user_from_id', 'user_to_id', 'status', 'date'];
}
