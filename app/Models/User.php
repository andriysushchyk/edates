<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password', 'birth_date', 'sex', 'city', 'about', 'weight', 'height', 'telnumber', 'avatar'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function likes()
    {
        return $this->hasMany(Like::class, 'liked_id');
    }

    public function transformLikers($likes)
    {
        return $likes->map(function($like) {
            $res = new \stdClass();
            $res->name = $like->liker->name;
            $res->years = $like->liker->years;
            $res->avatar = $like->liker->avatar;
            $res->city = $like->liker->city;
            $res->date = $like->created_at;
            $res->id = $like->id;
            return $res;
        });
    }

    public function getLikers()
    {
        $likes = $this->likes()->with('liker')->get();
        return $this->transformLikers($likes);
    }

    public function getMutualLikers()
    {
        $likes = $this->likes()->with('liker')->get();
        $likes = $likes->filter(function($like){
            return (Like::where('liker_id', $like->liked_id)
                    ->where('liked_id', $like->liker_id)->get()->count() > 0);
        });

        return $this->transformLikers($likes);

    }

    public function getYearsAttribute()
    {
        return $this->getAge($this->birth_date);
    }

    function getAge($then) {
        $then_ts = strtotime($then);
        $then_year = date('Y', $then_ts);
        $age = date('Y') - $then_year;
        if(strtotime('+' . $age . ' years', $then_ts) > time()) $age--;
        return $age;
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function gifts()
    {
        return $this->belongsToMany(Gift::class, "user_gifts", "user_to_id")->withPivot('user_from_id', 'created_at');
    }

    public function getMeetsType1()
    {
        return $this->hasMany(Meeting::class, "user_from_id")->orderByDesc("date");
    }

    public function getMeetsType2()
    {
        return $this->hasMany(Meeting::class, "user_to_id")->where('status', 0)->orderByDesc("date");
    }

    public function getMeetsType3()
    {
        return $this->hasMany(Meeting::class, "user_to_id")->orderByDesc("date");
    }

}
