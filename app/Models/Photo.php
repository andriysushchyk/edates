<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ["source", "user_id", 'description'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
