function openNav() {
    document.getElementById("mySidenav").style.width = "90%";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

function toggleNav() {
    var navbarWidth = document.getElementById("mySidenav").style.width;
    if (!parseInt(navbarWidth)) {
        openNav();
    }   else {
        closeNav();
    }
}