myApp.controller('giftsController', function($rootScope, $scope, $http) {

    $scope.gifts = [];


    $scope.loadGifts = function(lastItemId = false) {
        $scope.myPromise = $http({

            method  : 'GET',
            url     : '/api/v1/gifts',
            headers : {},
            params  : {
                token: $rootScope.getToken(),
                lastGiftID: lastItemId
            }

        }).then(function successCallback(response) {
            $scope.gifts = $scope.gifts.concat(response.data.data.gifts);
        }, function errorCallback(response) {
            if (response.status == 401) {
                $rootScope.onLogout();
            }   else {
                alert('Невдалось завантажити подарунки. Спробуйте ще.')
            }
        });
    };

    $scope.loadGifts();
});
