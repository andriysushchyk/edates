myApp.controller('likesController', function($rootScope, $scope, $http) {

    $scope.nowLikesIsMutual = false;

    $scope.switchLikes = function(isMutual) {
        $scope.nowLikesIsMutual = isMutual;
        $scope.nowLikes = isMutual ? $scope.mutualLikes : $scope.allLikes;
    };

    $scope.allLikes = [];
    $scope.mutualLikes = [];

    $scope.loadAllLikes = function() {
        var lastItemId = 0;
        if ($scope.allLikes.length) {
            lastItemId = $scope.allLikes[$scope.allLikes.length - 1].id;
        }
        $scope.myPromise = $http({

            method  : 'GET',
            url     : '/api/v1/likes',
            headers : {},
            params  : {
                token: $rootScope.getToken(),
                lastLikeID: lastItemId
            }

        }).then(function successCallback(response) {
            $scope.allLikes = $scope.allLikes.concat(response.data.data.users);
            $scope.nowLikes = $scope.allLikes;
            console.log($scope.allLikes);

        }, function errorCallback(response) {
            if (response.status == 401) {
                $rootScope.onLogout();
            }   else {
                alert('Невдалось завантажити симпатії. Спробуйте ще.')
            }
        });
    };

    $scope.loadMutualLikes = function() {
        var lastItemId = 0;
        if ($scope.allLikes.length) {
            lastItemId = $scope.allLikes[$scope.allLikes.length - 1].id;
        }
        $scope.myPromise = $http({

            method  : 'GET',
            url     : '/api/v1/likes',
            headers : {},
            params  : {
                token: $rootScope.getToken(),
                lastLikeID: lastItemId,
                mutual: 1
            }

        }).then(function successCallback(response) {

            $scope.mutualLikes = $scope.mutualLikes.concat(response.data.data.users);
            $scope.nowLikes = $scope.mutualLikes;

        }, function errorCallback(response) {
            if (response.status == 401) {
                $rootScope.onLogout();
            }   else {
                alert('Невдалось завантажити симпатії. Спробуйте ще.')
            }
        });
    };

    $scope.loadMoreLikes = function() {
        if ($scope.nowLikesIsMutual) {
            $scope.loadMutualLikes();
        }   else {
            $scope.loadAllLikes();
        }
    };
    $scope.loadAllLikes();
    $scope.loadMutualLikes();
    $scope.nowLikes = $scope.allLikes;

});
