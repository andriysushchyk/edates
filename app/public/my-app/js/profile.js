myApp.controller('profileController', function($rootScope, $scope, $http) {

    $scope.userProfile = {};

    $scope.myPromise = $http({
        method  : 'GET',
        url     : '/api/v1/user_profile',
        headers : {},
        params  : {token: $rootScope.getToken() }
    }).then(function successCallback(response) {
        var user = response.data.data.user;
        console.log(user);
        $scope.userProfile.name = user.name;
        $scope.userProfile.sex = user.sex;
        $scope.userProfile.city_id = user.city_id;
        $scope.userProfile.birth_date = user.birth_date;
        $scope.userProfile.telnumber = user.telnumber;
        $scope.userProfile.about = user.about;
        $scope.userProfile.height = user.height;
        $scope.userProfile.weight = user.weight;
        console.log($scope.userProfile.sex);

    }, function errorCallback(response) {
        console.log(response.status);
        if (response.status == 401) {
            $rootScope.onLogout();
        }
    });

    $scope.related = {};
    $scope.related.cities = [
        {'id': 1, 'title': 'Київ'},
        {'id': 2, 'title': 'Харків'},
        {'id': 3, 'title': 'Луцьк'},
        {'id': 4, 'title': 'Любомль'},
    ];

    $scope.saveMessage = {};
    $scope.saveMessage.showMessage = false;
    $scope.saveMessage.messageClass = 'success';
    $scope.saveMessage.messageText = '';

    $scope.save = function () {
        $scope.myPromise = $http({
            method  : 'POST',
            url     : '/api/v1/user_profile',
            data    : $scope.userProfile,
            params  : {token: $rootScope.getToken() },
            headers : {}
        }).then(function successCallback(response) {
            $scope.saveMessage.showMessage = true;
            $scope.saveMessage.messageClass = 'success';
            $scope.saveMessage.messageText = 'Зміни збережено.';

        }, function errorCallback(response) {

            $scope.saveMessage.showMessage = true;
            $scope.saveMessage.messageClass = 'danger';

            var statusCode = response.status;
            if (statusCode == 422) {
                $scope.saveMessage.messageText = 'Введено невірні дані.';
            }   else {
                $scope.saveMessage.messageText = 'Помилка сервера, спробуйте перезавантажити сторінку';
            }
        });
    }


});
