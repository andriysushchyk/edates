var myApp = angular.module('myApp', ['ui.router', 'cgBusy', 'ngMask', 'ui.bootstrap', 'sc.select', 'ui.select', 'ngSanitize' ]);

function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.offsetHeight + 'px';
}

function test() {
    var dd = document.getElementsByTagName("iframe");
    if (dd.length) {
        resizeIframe(dd[0]);
    }
}

setInterval('test()', 300);

myApp.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    var peopleState = {
        name: 'people',
        url: '/',
        templateUrl: '/my-app/templates/people.html',
        data: {
            authorization: true,
            redirectTo: 'welcome'
        }
    };

    var likesState = {
        name: 'likes',
        url: '/likes',
        templateUrl: '/my-app/templates/likes.html',
        data: {
            authorization: true,
            redirectTo: 'welcome'
        }
    };

    var meetingsState = {
        name: 'meetings',
        url: '/meetings',
        templateUrl: '/my-app/templates/meetings.html',
        data: {
            authorization: true,
            redirectTo: 'welcome'
        }
    };

    var giftsState = {
        name: 'gifts',
        url: '/gifts',
        templateUrl: '/my-app/templates/gifts.html',
        data: {
            authorization: true,
            redirectTo: 'welcome'
        }
    };

    var profileState = {
        name: 'profile',
        url: '/profile',
        templateUrl: '/my-app/templates/profile.html',
        data: {
            authorization: true,
            redirectTo: 'welcome'
        }
    };

    var settingsState = {
        name: 'settings',
        url: '/settings',
        templateUrl: '/my-app/templates/settings.html',
        data: {
            authorization: true,
            redirectTo: 'welcome'
        }
    };

    var welcomeState = {
        name: 'welcome',
        url: '/home',
        templateUrl: '/my-app/templates/welcome.html',
        data: {
            authorization: false
        },
        controller: function($scope, $state, Authorization, $http, $filter) {
            $scope.reguser = { birth_date: new Date(1991, 9, 24) };
            $scope.loguser = {};
            $scope.cities = {};

            $scope.login = function() {
                $http({
                    method  : 'POST',
                    url     : '/api/v1/auth/login',
                    data    : $scope.loguser,
                    headers : {}
                }).then(function successCallback(response) {
                    Authorization.saveToken(response.data.token);
                    $state.go('people');

                }, function errorCallback(response) {
                    var statusCode = response.status;
                    if (statusCode == 422) {
                        alert('Неправильний логін або пароль.');
                    }   else {
                        alert('Неправильний логін або пароль.');
                    }
                });
            };

            $scope.register = function () {
                $scope.reguser.birth_date = $filter('date')($scope.reguser.birth_date, "yyyy-MM-dd");
                $http({
                    method  : 'POST',
                    url     : '/api/v1/auth/register',
                    data    : $scope.reguser,
                    headers : {}
                }).then(function successCallback(response) {
                    console.log(response);
                    Authorization.saveToken(response.data.token);
                    $state.go('people');
                }, function errorCallback(response) {
                    var statusCode = response.status;
                    if (statusCode == 409) {
                        alert('Ця електронна адреса зайнята.');
                    }   else {
                        alert('Поля заповнені невірно.');
                    }
                });

            }

            $http({
                method: 'GET',
                url: '/get_cities',
            }).then(function successCallback(response) {
                $scope.cities.data = response.data;
            }, function errorCallback(response) {
                console.log("asdasd");
            });
        }
    };
    $stateProvider.state(peopleState);
    $stateProvider.state(likesState);
    $stateProvider.state(meetingsState);
    $stateProvider.state(giftsState);
    $stateProvider.state(profileState);
    $stateProvider.state(welcomeState);
    $stateProvider.state(settingsState);

}).run(['$rootScope', '$state', 'Authorization', function($rootScope, $state, Authorization) {

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        console.log('to State: ' + toState.name);
        console.log(Authorization.getToken());
        if (!(Authorization.isAuthorized())) {
            if (toState.data && toState.data.authorization) {
                $state.go(toState.data.redirectTo);
            }
        }   else {

        }

    });

    $rootScope.getToken = function() {
       return Authorization.getToken();
    };

    $rootScope.onLogout = function() {
        Authorization.clear();
        $state.go('welcome');
    };

}]).service('Authorization', ['$state', function($state) {

        isAuthorized = function() {
            return (localStorage.userCreds);
        };

        var clear = function() {
                localStorage.clear();
            },

            go = function(fallback) {
                var targetState = this.memorizedState ? this.memorizedState : fallback;
                $state.go(targetState);
            },
            saveTokenToStorage = function(userToken) {
                localStorage.userCreds = angular.toJson({'token' : userToken});
            },
            getTokenFromStorage = function () {
                if (localStorage.userCreds) {
                    var userCreds = angular.fromJson(localStorage.userCreds);
                    if ('token' in userCreds) {
                        return userCreds.token;
                    }
                }
                return false;
            };

        return {
            isAuthorized: isAuthorized,
            memorizedState: this.memorizedState,
            clear: clear,
            go: go,
            getToken: getTokenFromStorage,
            saveToken: saveTokenToStorage
        };
    }]).value('cgBusyDefaults',{
    backdrop: true,
    templateUrl: "/my-app/templates/preloader.html",
    delay: 0,
    minDuration: 1000,
    wrapperClass: 'myloader'
});
