<?php

namespace App\Http\Requests\Web;

use App\Http\Requests\Api\CheckUniqueEmailTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateUserProfileRequest extends FormRequest
{
    use CheckUniqueEmailTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'city' => 'required|max:100',
            'sex' => 'required|in:1,0',
            'telnumber' => 'max:20',
            'weight' => 'numeric|min:20|max:600',
            'height' => 'numeric|min:20|max:600',
        ];
    }

    public function validate()
    {
        $this->checkUniqueEmail($this->all(), Auth::user()->id);
        parent::validate();
    }
}
