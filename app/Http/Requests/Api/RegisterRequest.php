<?php

namespace App\Http\Requests\Api;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RegisterRequest extends FormRequest
{
    use CheckUniqueEmailTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'email' => 'required|email|unique:users',
            'password' => 'required|max:100',
            'city' => 'required|max:100',
            'birth_date' => 'required|date|date_format:Y-m-d',
            'sex' => 'required|in:1,0'
        ];
    }

    public function validate()
    {
        $this->checkUniqueEmail($this->all());
        parent::validate();
    }

}
