<?php

namespace App\Http\Requests\Api;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;

trait CheckUniqueEmailTrait
{
    /**
     * Mobile devs need 409 code, when email is reserved, but default validator returns 422.
     * This method return 409 code, when email is reserved.
     * @param $requestData
     * @param bool|int $exceptId
     */

    public function checkUniqueEmail($requestData, $exceptId = false) {


        $rule = 'unique:users,email';
        if ($exceptId !== false) {
            $rule .= ",$exceptId";
        }

        $customValidator = Validator::make($requestData,
            ['email' => $rule]
        );

        if ($customValidator->fails()) {
            throw new HttpResponseException(
                new JsonResponse($this->formatErrors($customValidator), 409)
            );
        }
    }
}