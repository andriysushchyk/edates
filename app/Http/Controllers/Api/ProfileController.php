<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\UpdateProfileRequest;
use App\Http\Requests\Api\UpdateUserProfileRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProfileController extends ApiController
{
    public function get($userId = false)
    {

        if ($userId != false) {
            $user = User::find($userId);
        }   else {
            $user = Auth::user();
        }
        if (!$user) {
            return $this->failResponse("User not found", 404);
        }

        $user->load('photos');
        $user->load('gifts');
        $user->load('gifts');
        $user->load('getMeetsType1');
        $user->load('getMeetsType2');
        $user->load('getMeetsType3');
        return $this->successResponse($user);
    }

    public function update(UpdateUserProfileRequest $request)
    {
        unset($request->password);
        $user = Auth::User();
        $user->update($request->all());
        return $this->successResponse($user);
    }

    public function search(Request $request)
    {
        $users = User::all();

        if (isset($request->city) && $request->city) {
            $users = $users->filter(function($user) use ($request) {
                return $user->city == $request->city;
            });
        }

        $sex = $request->sex ?? [(Auth::user()->sex == 1 ? 0 : 1)];
        $users = $users->filter(function($user) use ($sex) {
            return in_array($user->sex, $sex) && (Auth::user()->id != $user->id);
        });

        if (isset($request->age_from) && $request->age_from) {
            $users = $users->filter(function($user) use ($request) {
                return (((int) $user->years) >= (int)$request->age_from);
            });
        }


        if (isset($request->age_to) && $request->age_to) {
            $users = $users->filter(function($user) use ($request) {
                return (((int) $user->years) <= (int)$request->age_to);
            });
        }

        if ($request->ajax()) {
            return $this->successResponse(["users" => $users]);
        };

        $searchParameters = $request->all();
        return $this->successResponse($users);
    }

}
