<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use JWTAuthException;
use JWTAuth;

class ApiController extends Controller
{
    function successResponse($data = [], $statusCode = 200) {
        return response()
            ->json(['status' => 'success', 'data' => $data], $statusCode);
    }

    function failResponse($message, $statusCode = 500) {
        return response()
            ->json(['status' => 'failed', 'message' => $message], $statusCode);
    }
}
