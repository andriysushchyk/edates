<?php

namespace App\Http\Controllers\Api;

use App\Models\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class PhotosController extends ApiController
{
    public function store(Request $request)
    {

        if(!$request->file("avatar") || !$request->description) {
            return $this->failResponse("Fields 'avatar' and 'description' are required", 422);
        }
        $mimetype = $request->file("avatar")->getMimeType();

        if ($mimetype != "image/png" && $mimetype != "image/jpg" && $mimetype != "image/jpeg") {
            return $this->failResponse("Bad format of file", 401);
        }

        $avatarFile =  $request->avatar;
        $folder = '/uploads/photos';
        $filename = time() . '.' . $avatarFile->extension();
        $avatarFile->move(public_path() . $folder, $filename);

        $photo = Photo::create([
            'description' => $request->description,
            'source' => "$folder/$filename",
            'user_id' => Auth::user()->id
        ]);
        return $this->successResponse(['photo' => $photo]);
    }

    public function update(Request $request, Photo $photo)
    {
        if ($request->description) {
            $photo->update(['description' => $request->description]);
            return $this->successResponse(['photo' => $photo]);
        }

        return $this->failResponse("Bad value of 'description' field", 400);

    }

    public function get()
    {
        return $this->successResponse(['photos' => Auth::user()->photos]);
    }

    public function delete(Photo $photo)
    {
        $photo->delete();
        return $this->successResponse();
    }
}
