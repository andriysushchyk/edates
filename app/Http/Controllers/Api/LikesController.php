<?php

namespace App\Http\Controllers\Api;

use App\Models\Like;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LikesController extends ApiController
{
    public function setLike(User $user)
    {
        $likedId = $user->id;
        $likerId = Auth::user()->id;

        if ($likedId == $likerId) {
            return $this->failResponse("Self-asigned like");
        }

        if (Like::where('liked_id', $likedId)->where('liker_id', $likerId)->get()->count()) {
            return $this->failResponse("This user already liked by user", 409);
        }

        Like::create([
            'liked_id' => $likedId,
            'liker_id' => $likerId,
        ]);
        return $this->successResponse();
    }

    public function unsetLike(User $user)
    {
        $likedId = $user->id;
        $likerId = Auth::user()->id;
        Like::where('liked_id', $likedId)->where('liker_id', $likerId)->delete();
        return $this->successResponse();
    }

    public function getMyLikes(Request $request)
    {
        if ($request->mutual == true) {
            $likes = Auth::user()->getMutualLikers();
        }   else {
            $likes = Auth::user()->getLikers();
        }
        $likes = $likes->sortByDesc('date');

        if ($request->lastLikeID != false) {
            $likeDate = Like::find($request->lastLikeID)->created_at;
            $likes = $likes->filter(function ($like) use ($likeDate) {
               return $likeDate > $like->date;
            });
        }
        $likes = $likes->slice(0, 1)->values();

        return $this->successResponse(['users' => $likes]);
    }
}
