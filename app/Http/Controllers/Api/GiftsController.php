<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GiftsController extends ApiController
{
    public function send(Request $request)
    {
        DB::table('user_gifts')->insert(
            [
                'user_to_id' => $request->user_id,
                'user_from_id' => Auth::user()->id,
                'gift_id' => $request->gift_id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );

        return $this->successResponse();
    }


}
