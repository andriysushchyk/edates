<?php

namespace App\Http\Controllers\Api;

use App\Models\Meeting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class MeetingsController extends ApiController
{
    public function send(Request $request)
    {
        $date = Carbon::parse($request->date);
        if ($date && $date > Carbon::parse("now")) {
            Meeting::create([
                'user_to_id' => $request->user_id,
                'user_from_id' => Auth::user()->id,
                'status' => 0,
                'date' => $date
            ]);
            return $this->successResponse();
        }

        return $this->failResponse("", 400);
    }

    public function changeStatus(Request $request, Meeting $meeting)
    {
        $meeting->update(['status' => $request->status]);
        return $this->successResponse();
    }
}
