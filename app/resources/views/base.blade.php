<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>eDates</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ asset('node_modules/bootstrap/dist/css/bootstrap.css') }}">
    <script src="{{ asset('node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('node_modules/jquery-ui-dist/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('node_modules/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('node_modules/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/welcome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('node_modules/jquery-ui-dist/jquery-ui.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&amp;subset=cyrillic-ext"
          rel="stylesheet">
    @yield('includes')
</head>
<body>
<header id="header">
    <div class="container-fluid">
        <div class="row header-row">
            <div class="col-md-4 col-xs-12 title-column">
                <img class="logo" src="{{ asset('images/logo.png') }}">
                <h1 class="site-name">eDates</h1>
            </div>
            <div class="col-md-offset-4 col-md-4 col-xs-12 share-buttons-column">
                <a href="http://www.facebook.com/sharer.php?u=http://kursa4.my/" target="_blank">
                    <img class="share-button" src="https://simplesharebuttons.com/images/somacro/facebook.png"
                         alt="Facebook"/>
                </a>
                <a href="http://vkontakte.ru/share.php?url=https://simplesharebuttons.com" target="_blank">
                    <img class="share-button" src="https://simplesharebuttons.com/images/somacro/vk.png" alt="VK"/>
                </a>
                <!-- Twitter -->
                <a href="https://twitter.com/share?url=https://simplesharebuttons.com&amp;text=Simple%20Share%20Buttons&amp;hashtags=simplesharebuttons"
                   target="_blank">
                    <img class="share-button" src="https://simplesharebuttons.com/images/somacro/twitter.png"
                         alt="Twitter"/>
                </a>
            </div>
        </div>
    </div>
</header>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-md-5 col-md-push-6 col-md-offset-1 col-site-definition">
            <h3 class="site-definition">Безкоштовні знайомства</h3>
        </div>
        <div class="col-md-5 col-md-offset-1 col-md-pull-6 col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="panel panel-info" id="auth-box">
                <div class="panel-heading">
                    <div class="row text-center">
                        <div class="col-xs-6 col-sign-in" onclick="$('#login').show();$('#register').hide()">Вхід</div>
                        <div class="col-xs-6 col-sign-up" onclick="$('#register').show();$('#login').hide()">Реєстрація</div>
                    </div>
                </div>
                <div class="panel-body" id="login" style="display:none">
                    <form id="loginform" class="form-horizontal" role="form">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                            <input type="text" class="form-control" name="name" value="" placeholder="E-mail">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" class="form-control" name="password" placeholder="Пароль">
                        </div>
                        <div class="input-group">
                            <div class="checkbox">
                                <label>
                                    <input id="login-remember" type="checkbox" name="remember" value="1">Запам'ятати
                                    мене
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                <a id="btn-login" href="#" class="btn btn-success">Увійти</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-body" id="register">
                    <form id="signupform" class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="firstname" class="col-md-3 control-label">Ім'я</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="name" placeholder="Андрій">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname" class="col-md-3 control-label">Ви</label>
                            <div class="col-md-9">
                                <label class="radio-inline"><input type="radio" name="optradio">Хлопець</label>
                                <label class="radio-inline"><input type="radio" name="optradio">Дівчина</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="birthday" class="col-md-3 control-label">Дата народження</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="birthday" placeholder="01/01/1990">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname" class="col-md-3 control-label">Місто</label>
                            <div class="col-md-9">
                                <select class="form-control selectpicker" data-live-search="true">
                                    <option>Київ</option>
                                    <option data-tokens="mustard">Харків</option>
                                    <option data-tokens="frosting">Запоріжжя</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-3 control-label">Email</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="email" placeholder="user@gmail.com">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-3 control-label">Пароль</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="password" placeholder="*******">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                <button id="btn-signup" type="button" class="btn btn-info"><i
                                            class="icon-hand-right"></i> &nbsp Зареєструватись
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    $(document).ready(function () {
        $("#birthday").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '-100:+0',
            defaultDate: '01/01/1990'
        });
    });
</script>
</html>