@extends("pages.base")
@section('head')
    <style>
        .page-title {
            text-align: center;
            font-weight: bold;
            margin: 15px 0;
        }
    </style>
@endsection
@section("content")
    <div class="container">
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert"
                       aria-label="close">&times;</a> {{ session('status') }}
                </div>
            @endif
            <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <form name="save_profile_form" id="save_profile_form" method="post" class="form-horizontal"
                      action="/pages/profile/update?token={{ \Tymon\JWTAuth\Facades\JWTAuth::getToken() }}">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="name">Ім'я:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Антон" required
                                   value="{{ old("name") ?? $user->name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Ви</label>
                        <div class="col-sm-9">
                            <label class="radio-inline">
                                <input type="radio" name="sex" value="1" {{ $user->sex == "1" ? " checked " : " " }}>Хлопець
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="sex" value="0" {{ $user->sex == "0" ? " checked " : " " }}>Дівчина
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="birthday" class="col-sm-3 control-label">Дата народження</label>
                        <div class="col-sm-9">
                            <input class="form-control" style="width: 15%; display: inline" type="number" min="1"
                                   max="31"
                                   name="day" value="{{ old("day") ?? intval(substr($user->birth_date, 8, 2)) }}"> -
                            <input class="form-control" style="width: 15%; display: inline" type="number" min="1"
                                   max="12"
                                   name="month" value="{{ old("month") ?? intval(substr($user->birth_date, 5, 2)) }}"> -
                            <input class="form-control" style="width: 20%; display: inline" type="number" min="1950"
                                   max="2005"
                                   name="year" value="{{ old("year") ?? substr($user->birth_date, 0, 4) }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Місто</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="city" form="save_profile_form">
                                @foreach($cities as $city)
                                    <option value="{{ $city->name }}" {{ $city->name == $user->city ? " selected " : "" }}>
                                        {{ $city->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Номер телефону</label>
                        <div class="col-sm-9">
                            <input class="form-control" ng-model="userProfile.telnumber" name="telnumber" id="telnumber"
                                   pattern="[0-9]{10,12}" placeholder="0971447236"
                                   value="{{ old("telnumber") ?? $user->telnumber }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Про себе</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" ng-model="userProfile.about" maxlength="250"
                                      ng-maxlength="250" placeholder="Бла-бла-бла" name="about" form="save_profile_form"
                                      rows="5">{{ $user->about }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="height" class="col-sm-3 control-label">Ріст (cм)</label>
                        <div class="col-sm-9">
                            <input type="number" ng-model="userProfile.height"
                                   class="form-control" min="20" max="250" type="number" value="{{ $user->height }}"
                                   id="height" name="height">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="weight" class="col-sm-3 control-label">Вага (кг)</label>
                        <div class="col-sm-9">
                            <input type="number" min="20" max="250" value="{{ $user->weight }}"
                                   class="form-control" id="weight" name="weight">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 text-center">
                            <button type="submit" class="btn btn-default">
                                Зберегти
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if (session('avatar_status'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert"
               aria-label="close">&times;</a> {{ session('avatar_status') }}
        </div>
    @endif
    <div class="container">
        <div class="row page-title">
            <h2><span class="glyphicon glyphicon-picture"></span> Аватар</h2>
        </div>
        <div class="row text-center">
            <img src="{{ $user->avatar }}" height='200px' width="auto">
        </div>
        <form method="post" enctype="multipart/form-data" action="/pages/profile/update_avatar?token={{ \Tymon\JWTAuth\Facades\JWTAuth::getToken() }}">
            <div class="row text-center">
                <input type="file" style="display: inline" name="avatar">
            </div>
            <div class="row text-center">
                <button type="submit" class="btn btn-default">
                    Оновити
                </button>
            </div>
        </form>
    </div>
@endsection
@section('after-body')
    <script>
        $(document).ready(function () {
            $("#birthday").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '-100:+0',
                defaultDate: '01/01/1990'
            });
        });
    </script>
@endsection