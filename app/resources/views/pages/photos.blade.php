@extends("pages.base")
@section("head")
    <style>
        body {
            min-height: 500px;
        }

        .one-photo {
            height: 340px;
        }
        .user-photo-wrapper {
            max-height: 320px;
            text-align: center;
        }

        .user-photo {
            height: auto;
            max-height: 300px;
            width: auto;
            max-width: 100%;
        }

        .photo-action {
            display: inline-block;
            text-align: center;
            cursor: pointer;
        }

        .edit {
            width: 50%;
        }

        .delete {
            width: 50%;
        }
    </style>
@endsection
@section("content")
<div class="container">
    <div class="row text-right">
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#feedback-modal">
            <span class="glyphicon glyphicon-camera"></span> Додати нове фото
        </button>
    </div>
    <div class="row" id="photos" style="margin-top: 6px">

    </div>
    <div id="feedback-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="close" data-dismiss="modal">×</a>
                    <h3>Додати нове фото</h3>
                </div>
                <form method="POST" enctype="multipart/form-data" id="fileUploadForm">
                <div class="modal-body">
                        <div class="form-group">
                            <label>Опис:</label>
                            <textarea rows="3" class="form-control" form="fileUploadForm" id="description" name="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Зав. форму: </label>
                            <input type="file" id="avatar" name="avatar"/><br/><br/>
                        </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-success"  id="btnSubmit" type="button">Додати</button>
                    <a href="#" class="btn" data-dismiss="modal">Скасувати</a>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section("after-body")
    <script>
        function renderPhotos(photos) {
            var container = $("#photos");
            container.empty();
            photos.forEach(function (photo) {
                container.append(
                    "<div class='col-md-4 one-photo' style='margin-bottom: 20px'>" +
                        "<div class='user-photo-wrapper'>" +
                            '<img class="user-photo" src="' + photo.source + '">' +
                        "</div>" +
                            "<div onclick='editPhoto()' id='" + photo.id + "' class='photo-action edit'><span class='glyphicon glyphicon-edit'></span>Редагувати</div>" +
                            "<div onclick='deletePhoto(" + photo.id + ")' id='" + photo.id + "' class='photo-action delete'><span class='glyphicon glyphicon-edit'></span>Видалити</div>" +
                    "</div>");
            });
        }

        function clearForm() {
            $("#avatar").val("");
            document.getElementById("description").value = "";
        }

        function editPhoto() {
            alert("Ця функція наразі недоступна");
        }

        function deletePhoto(id)
        {
            $.ajax({
                type: "DELETE",
                url: "/api/v1/photos/" + id + "?token={{ \Tymon\JWTAuth\Facades\JWTAuth::getToken() }}",
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    alert("Фото видалено");
                    reloadPhotos();
                },
                error: function (e) {
                    alert("Не вдалось видалити фото.");
                }
            });
        }

        function reloadPhotos() {
            $.ajax({
                type: "GET",
                url: "/api/v1/photos?token={{ \Tymon\JWTAuth\Facades\JWTAuth::getToken() }}",
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    renderPhotos(data.data.photos);
                },
                error: function (e) {
                    alert(e.responseJSON.message);
                }
            });
        }

        $(document).ready(function () {

            reloadPhotos();

            $("#btnSubmit").click(function (event) {

                //stop submit the form, we will post it manually.
                event.preventDefault();

                // Get form
                var form = $('#fileUploadForm')[0];

                // Create an FormData object
                var data = new FormData(form);

                // If you want to add an extra field for the FormData
                data.append("CustomField", "This is some extra data, testing");

                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "/api/v1/photos?token={{ \Tymon\JWTAuth\Facades\JWTAuth::getToken() }}",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
                         alert("Фото додано");
                         $('#feedback-modal').modal('hide');
                         reloadPhotos();
                         clearForm();

                    },
                    error: function (e) {
                        clearForm();
                        if (e.responseJSON) {
                            alert("Не вдалось завантажити фото: " + e.responseJSON.message);
                        }   else {
                            alert("Не вдалось завантажити фото.")
                        }
                    }
                });
            });
        });
    </script>
@endsection