@extends("pages.base")
@section('head')
    <style>
        .action-column h4 {
            line-height: 80px;
            height: 80px;
        }

        .action-button-column {
            margin-top: 30px;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-5">
                <div class="row">
                    <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#home">Запрошені мною</a></li>
                        <li><a data-toggle="pill" href="#menu1">Нові запрошення</a></li>
                        <li><a data-toggle="pill" href="#menu2">Всі запрошення</a></li>
                    </ul>
                </div>
            </div>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    @forelse(\Illuminate\Support\Facades\Auth::user()->getMeetsType1 as $item)
                        <div class="row">
                            <div class="col-sm-8 col-xs-12 text-center">
                                <h2>{{ \App\Models\User::find($item->user_to_id)->name }}
                                    , {{ \App\Models\User::find($item->user_to_id)->years }}</h2>
                                <h5>
                                    <span class="glyphicon glyphicon-time"></span> {{ \Carbon\Carbon::parse($item->date)->format("d.m.Y") }}
                                    &nbsp; &nbsp;
                                    <span class="glyphicon glyphicon-phone"></span>
                                    {{ $item->status  == 1 ? \App\Models\User::find($item->user_to_id)->telnumber ?? "не вказано" : "недоступно" }}
                                </h5>
                            </div>
                            <div class="col-sm-4 col-xs-12 action-column text-center"><h4>
                                    @if ($item->status == 1)
                                        Прийнято
                                    @elseif($item->status == 0)
                                        В очікуванні
                                    @else
                                        Відхилено
                                    @endif
                                </h4></div>
                        </div>
                    @empty
                        <div class="col-xs-12">
                            <h3 class="text-center"> Запрошень не знайдено </h3>
                        </div>
                    @endforelse
                </div>
                <div id="menu1" class="tab-pane fade">
                    @forelse(\Illuminate\Support\Facades\Auth::user()->getMeetsType2 as $item)
                        <div class="row">
                            <div class="col-sm-8 col-xs-12 text-center">
                                <h2>{{ \App\Models\User::find($item->user_from_id)->name }}
                                    , {{ \App\Models\User::find($item->user_from_id)->years }}</h2>
                                <h5>
                                    <span class="glyphicon glyphicon-time"></span> {{ \Carbon\Carbon::parse($item->date)->format("d.m.Y") }}
                                    &nbsp;
                                    <span class="glyphicon glyphicon-phone"></span> 09715582
                                </h5>
                            </div>
                            <div class="col-sm-2 col-xs-12">
                                <button onclick="changeStatus({{ $item->id }}, 1)"
                                        class="btn btn-success action-button-column button{{ $item->id }}">
                                    <h5>Прийняти</h5>
                                </button>
                            </div>
                            <div class="col-sm-2 col-xs-12 action-button-column">
                                <button  onclick="changeStatus({{ $item->id }}, -1)" class="btn btn-warning button{{ $item->id }}">
                                    <h5>Відмінити</h5>
                                </button>
                            </div>
                        </div>
                    @empty
                        <div class="col-xs-12">
                            <h3 class="text-center"> Запрошень не знайдено </h3>
                        </div>
                    @endforelse
                </div>
                <div id="menu2" class="tab-pane fade">
                    @forelse(\Illuminate\Support\Facades\Auth::user()->getMeetsType3 as $item)
                        <div class="row">
                            <div class="col-sm-8 col-xs-12 text-center">
                                <h2>{{ \App\Models\User::find($item->user_from_id)->name }}
                                    , {{ \App\Models\User::find($item->user_from_id)->years }}</h2>
                                <h5>
                                    <span class="glyphicon glyphicon-time"></span> {{ \Carbon\Carbon::parse($item->date)->format("d.m.Y") }}
                                    &nbsp; &nbsp;
                                    <span class="glyphicon glyphicon-phone"></span>
                                    {{ \App\Models\User::find($item->user_from_id)->telnumber ?? "не вказано" }}
                                </h5>
                            </div>
                            <div class="col-sm-4 col-xs-12 action-column text-center"><h4>
                                    @if ($item->status == 1)
                                        Прийнято
                                    @elseif($item->status == 0)
                                        В очікуванні
                                    @else
                                        Відхилено
                                    @endif
                                </h4></div>
                        </div>
                    @empty
                        <div class="col-xs-12">
                            <h3 class="text-center"> Запрошень не знайдено </h3>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <div id="home" class="tab-pane fade in active">
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <h3>Menu 1</h3>
                        <p>Some content in menu 1.</p>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <h3>Menu 2</h3>
                        <p>Some content in menu 2.</p>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section("after-body")
            <script>
                function changeStatus(id, status) {
                    event.preventDefault();
                    // Get form
                    var form = $('#fileUploadForm')[0];

                    // Create an FormData object
                    var data = new FormData(form);

                    data.append("status", status);

                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: "/api/v1/meetings/change_status/" + id + "?token={{ \Tymon\JWTAuth\Facades\JWTAuth::getToken() }}",
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        timeout: 600000,
                        success: function (data) {
                            if (status == 1) {
                                alert("Запрошення прийнято");
                            }   else {
                                alert("Запрошення відхилено");
                            }
                            $(".button" + id).attr("disabled", true);
                        },
                        error: function (e) {
                            alert("Не вдалось відправити подарунок.");
                        }
                    });
                }
            </script>
@endsection