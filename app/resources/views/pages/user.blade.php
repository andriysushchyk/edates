@extends("pages.base")
@section('head')

    <link href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.5.3/datepicker.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.5.3/datepicker.css" rel="stylesheet"> <!-- 3 KB -->
    <style>
        .action {
            margin-top: 10px;
            width: 120px;
        }

        @media (max-width: 768px) {

            .def {
                text-align: center !important;
            }
        }

        .fotorama__img {
            max-width: 80% !important;
            margin: auto;
        }

        .gift-image {
            width: 40px;
            height: 40px;
            margin-left: 10px;
        }

    </style>
@endsection
@section('content')
    <div id="feedback-modal2" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="close" data-dismiss="modal">×</a>
                    <h3>Запрошення на зустріч</h3>
                </div>
                <form method="POST" enctype="multipart/form-data" id="fileUploadForm2">
                    <div class="form-group">
                        <div class="row">

                        <div class="col-xs-4 col-xs-offset-4 text-center">
                            <label>Вкажіть дату:</label>
                            <input class="form-control" name="date" type="date" id="date">
                        </div>
                        </div>
                    </div>
                    <div style="padding: 9px">
                        Коли користувач погодиться на зустріч, вам відкриються його контактні дані, і ви зможете
                        домовитись про деталі.
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnSubmit2" type="button">Відправити</button>
                        <a href="#" class="btn" data-dismiss="modal">Скасувати</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="feedback-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="close" data-dismiss="modal">×</a>
                    <h3>Відправка подарунку</h3>
                </div>
                <form method="POST" enctype="multipart/form-data" id="fileUploadForm">
                    <div class="form-group">
                        <select class="form-control" id="gifts">
                            @foreach(\App\Models\Gift::all() as $gift)
                                <option value="{{ $gift->id }}"
                                        style="background-image:url({{ $gift->source }});"> {{ $gift->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="gift-image">
                        <img src="{{ \App\Models\Gift::all()->first()->source }}" style="margin-left: 20px"
                             height="30px" width="30px">
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnSubmit" type="button">Відправити</button>
                        <a href="#" class="btn" data-dismiss="modal">Скасувати</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-3" style="text-align: right">
                <img src="{{ $user->avatar }}" style="max-height: 120px"
                     class="img-circle">
            </div>

            <div class="col-sm-6 text-center">
                <h2>{{ $user->name }}</h2>
                <h3><i>{{ $user->city }}, {{ $user->years }}</i></h3>
            </div>

            <div class="col-sm-3">
                <div class="action">
                    <button id="like" onclick="setLike()" type="button" class="btn btn-success"
                            style="background-color: rosybrown">
                        <span class="glyphicon glyphicon-heart-empty"></span> Відправити симпатію
                    </button>
                </div>
                <div class="action">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#feedback-modal2">
                        <span class="glyphicon glyphicon-map-marker"></span> Запросити на зустріч
                    </button>
                </div>
                <div class="action">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#feedback-modal">
                        <span class="glyphicon glyphicon-gift"></span> Надіслати подарунок
                    </button>
                </div>
            </div>
        </div>
        <div class="row def">
            <div class="col-sm-6 text-right def">
                <h4>Дата народження:</h4>
            </div>
            <div class="col-sm-6 text-left def">
                <h4>{{ substr($user->birth_date, 0, 10) }}</h4>
            </div>
        </div>
        <div class="row def">
            <div class="col-sm-6 text-right def">
                <h4>Ріст:</h4>
            </div>
            <div class="col-sm-6 text-left def">
                <h4>{{ $user->height ?? 'Не вказано' }}</h4>
            </div>
        </div>
        <div class="row def">
            <div class="col-sm-6 text-right def">
                <h4>Вага:</h4>
            </div>
            <div class="col-sm-6 text-left def">
                <h4>{{ $user->weight ?? 'Не вказано' }}</h4>
            </div>
        </div>
        <div class="row def">
            <div class="col-sm-6 text-right def">
                <h4>Про себе:</h4>
            </div>
            <div class="col-sm-6 text-left def">
                <h4>не вказано</h4>
            </div>
        </div>
    </div>

    <!-- 2. Add images to <div class="fotorama"></div>. -->
    <h3 class="text-center">Фотоальбом:</h3>
    <div class="text-center" style="display: flex; justify-content: center;">
        <div class="fotorama">
            @foreach ($user->photos as $photo)
                <img style="max-width: 80% !important; margin: auto;" src="{{ $photo->source }}">
            @endforeach
        </div>
    </div>
@endsection
@section('after-body')
    <script>
        function setLike() {
            $.ajax({
                type: "POST",
                url: "/api/v1/likes/{{ $user->id }}?token={{ \Tymon\JWTAuth\Facades\JWTAuth::getToken() }}",
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    alert("Cимпатію відправлено");
                    $('#like').attr("disabled", true);

                },
                error: function (e) {
                    alert("Cимпатію відправлено");
                    $('#like').attr("disabled", true);
                }
            });

        }

        var gifts = {!! \App\Models\Gift::all()->toJson()  !!};
        console.log(gifts);
        $(document).ready(function () {

            $("#datepicker").datepicker();

            $("#gifts").change(function () {
                $(".gift-image").empty();
                var source = "";
                gifts.forEach(function (gift) {
                    if (gift.id == $("#gifts").val()) {
                        source = gift.source;
                    }
                });
                $(".gift-image").append("<img src='" +
                    source + "' class='gift-image'>");

            });

            $("#btnSubmit2").click(function (event) {
                // Get form
                var form = $('#fileUploadForm2')[0];

                // Create an FormData object
                var data = new FormData(form);

                data.append("date", $("#date").val());
                data.append("user_id", {{ $user->id }});
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "/api/v1/meetings/send?token={{ \Tymon\JWTAuth\Facades\JWTAuth::getToken() }}",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
                        alert("Запрошення відправлено!");
                        $('#feedback-modal2').modal('hide');

                    },
                    error: function (e) {
                        alert("Вказана невірна дата");
                    }
                });

            });

            $("#btnSubmit").click(function (event) {

                //stop submit the form, we will post it manually.
                event.preventDefault();

                // Get form
                var form = $('#fileUploadForm')[0];

                // Create an FormData object
                var data = new FormData(form);

                data.append("gift_id", $("#gifts").val());
                data.append("user_id", {{ $user->id }});

                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "/api/v1/gifts/send?token={{ \Tymon\JWTAuth\Facades\JWTAuth::getToken() }}",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
                        alert("Подарунок відправлено");
                        $('#feedback-modal').modal('hide');

                    },
                    error: function (e) {
                        alert("Не вдалось відправити подарунок.");
                    }
                });
            });
        });
    </script>
@endsection