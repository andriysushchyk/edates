@extends("pages.base")
@section("head")
    <style>
        .age-input {
            width: 40%;
            display: inline;
        }

        .checkbox {
            margin-top: 0;
        }

        .panel > .list-group .list-group-item:first-child {
            /*border-top: 1px solid rgb(204, 204, 204);*/
        }

        @media (max-width: 767px) {
            .visible-xs {
                display: inline-block !important;
            }

            .block {
                display: block !important;
                width: 100%;
                height: 1px !important;
            }

            li.list-group-item img {
                margin: auto;
            }

            li.list-group-item {
                text-align: center;
            }

        }

        #back-to-bootsnipp {
            position: fixed;
            top: 10px;
            right: 10px;
        }

        .c-search > .form-control {
            border-radius: 0px;
            border-width: 0px;
            border-bottom-width: 1px;
            font-size: 1.3em;
            padding: 12px 12px;
            height: 44px;
            outline: none !important;
        }

        .c-search > .form-control:focus {
            outline: 0px !important;
            -webkit-appearance: none;
            box-shadow: none;
        }

        .c-search > .input-group-btn .btn {
            border-radius: 0px;
            border-width: 0px;
            border-left-width: 1px;
            border-bottom-width: 1px;
            height: 44px;
        }

        .c-list {
            padding: 0px;
            min-height: 44px;
        }

        .title {
            display: inline-block;
            font-size: 1.7em;
            font-weight: bold;
            padding: 5px 15px;
        }

        ul.c-controls {
            list-style: none;
            margin: 0px;
            min-height: 44px;
        }

        ul.c-controls li {
            margin-top: 8px;
            float: left;
        }

        ul.c-controls li a {
            font-size: 1.7em;
            padding: 11px 10px 6px;
        }

        ul.c-controls li a i {
            min-width: 24px;
            text-align: center;
        }

        ul.c-controls li a:hover {
            background-color: rgba(51, 51, 51, 0.2);
        }

        .c-toggle {
            font-size: 1.7em;
        }

        .name {
            font-size: 1.7em;
            font-weight: 700;
        }

        .c-info {
            padding: 5px 2px;
            font-size: 1.25em;
        }

    </style>
@endsection
@section("content")
    <div class="container">
        <div class="row">
            <form action="/api/v1/users/search">
                <div class="col-sm-3 col-xs-12">
                    <div class="form-group text-center">
                        <label class="control-label">Місто:</label>
                        <input type="hidden" value="{{ \Tymon\JWTAuth\Facades\JWTAuth::getToken() }}" name="token">
                        <select class="form-control" name="city">
                            <option value=""></option>
                            @foreach(\App\Models\City::getAll() as $city)
                                <option value="{{ $city->name }}" {{ (@$searchParameters['city'] == $city->name) ? ' selected ' : "" }}>
                                    {{ $city->name }}</option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="form-group text-center">
                        <label class="control-label">Вік:</label><br>
                        <input type="number" class="form-control age-input" name="age_from"
                               value="{{ @$searchParameters['age_from'] }}"> - <input type="number"
                                                                                      class="form-control age-input"
                                                                                      name="age_to"
                                                                                      value="{{ @$searchParameters['age_to'] }}">
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <div class="text-left" style="padding-left: 20px">
                        <label>Стать:</label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="sex[]" value="1"
                                    @if(isset($searchParameters['sex']))
                                        {{ (in_array(1,  $searchParameters['sex'])) ? " checked " : "" }}
                                    @else
                                        {{ (Auth::user()->sex == "0")  ? " checked " : "" }}
                                   @endif
                           >Чоловіча
                       </label><br>
                       <label>
                           <input type="checkbox" name="sex[]" value="0"
                           @if(isset($searchParameters['sex']))
                               {{ (in_array(0,  $searchParameters['sex'])) ? " checked " : "" }}
                                   @else
                               {{ (Auth::user()->sex == "1")  ? " checked " : "" }}
                                   @endif
                           >Жіноча
                        </label>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <div class="text-left" style="padding-left: 20px">
                        <label>Додатково:</label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox">З аватаром
                        </label><br>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <h3 style="cursor: pointer">
                        <button type="submit" class=" form-control btn-default btn-success">
                            <span class="glyphicon glyphicon-search"></span> Пошук
                        </button>
                    </h3>
                </div>
            </form>
        </div>
        <div class="row">
            @if (isset($users))
                @if (count($users) == 0)
                    <h3 class="text-center">За вашим запитом нікого не знайдено.</h3>
                @else
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                                <div class="panel panel-default">
                                    <div class="panel-heading c-list">
                                        <span class="title">Знайдені користувачі</span>
                                        <ul class="pull-right c-controls">
                                        </ul>
                                    </div>

                                    <div class="row" style="display: none;">
                                        <div class="col-xs-12">
                                            <div class="input-group c-search">
                                                <input type="text" class="form-control" id="contact-list-search">
                                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><span
                                            class="glyphicon glyphicon-search text-muted"></span></button>
                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="list-group" id="contact-list">
                                        @foreach($users as $user)
                                            <li class="list-group-item">
                                                <div class="col-xs-12 col-sm-3">
                                                    <img src="{{ $user->avatar }}" style="max-height: 100px"
                                                         alt="Scott Stevens" class="img-responsive img-circle"/>
                                                </div>
                                                <div class="col-xs-12 col-sm-9 text-center">
                                                    <div class="text-center">
                                                        <a href="{{ route('profile', ['user' => $user,
                                                         ]) . "?token=" . \Tymon\JWTAuth\Facades\JWTAuth::getToken() }}"><span class="name">{{ $user->name }}
                                                            , {{ $user->years }}</span><br/></a>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4">
                                                        <span class="glyphicon glyphicon-map-marker text-muted c-info"></span> {{ $user->city }}
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4">
                                                        <span class="glyphicon glyphicon-camera text-muted c-info"></span> {{ $user->photos->count() }}
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4">
                                                        <span class="glyphicon glyphicon-heart-empty text-muted c-info"></span> {{ $user->likes->count() }}
                                                    </div>
                                                    <div class="col-xs-12" style="padding-top: 12px">
                                                        <p>{{ $user->about }}</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- JavaScrip Search Plugin -->
                        <script src="//rawgithub.com/stidges/jquery-searchable/master/dist/jquery.searchable-1.0.0.min.js"></script>
                    </div>
                @endif
            @endif
        </div>
    </div>
@endsection
@section('after-body')
    <script>
        $(function () {
            /* BOOTSNIPP FULLSCREEN FIX */
            if (window.location == window.parent.location) {
                $('#back-to-bootsnipp').removeClass('hide');
            }


            $('[data-toggle="tooltip"]').tooltip();

            $('#fullscreen').on('click', function (event) {
                event.preventDefault();
                window.parent.location = "http://bootsnipp.com/iframe/4l0k2";
            });
            $('a[href="#cant-do-all-the-work-for-you"]').on('click', function (event) {
                event.preventDefault();
                $('#cant-do-all-the-work-for-you').modal('show');
            })

            $('[data-command="toggle-search"]').on('click', function (event) {
                event.preventDefault();
                $(this).toggleClass('hide-search');

                if ($(this).hasClass('hide-search')) {
                    $('.c-search').closest('.row').slideUp(100);
                } else {
                    $('.c-search').closest('.row').slideDown(100);
                }
            })

            $('#contact-list').searchable({
                searchField: '#contact-list-search',
                selector: 'li',
                childSelector: '.col-xs-12',
                show: function (elem) {
                    elem.slideDown(100);
                },
                hide: function (elem) {
                    elem.slideUp(100);
                }
            })
        });
    </script>
@endsection