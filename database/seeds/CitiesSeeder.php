<?php

use App\Models\City;
use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            'Київ',
            'Луцьк',
            'Чернігів',
            'Дніпро',
            'Черкаси',
        ];
        foreach ($cities as $city) {
            City::create([
               'name' => $city
            ]);
        }
    }
}
