<?php

use App\Models\Gift;
use Illuminate\Database\Seeder;

class GiftsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gifts = [
          ['title' => 'Frog', 'source' => '/uploads/gifts/frog.png'],
          ['title' => 'Turtle', 'source' => '/uploads/gifts/turtle.png'],
        ];

        foreach($gifts as $gift) {
            Gift::create($gift);
        }
    }
}
