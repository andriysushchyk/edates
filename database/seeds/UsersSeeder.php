<?php

use App\Models\Photo;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Андрій', 'sex' => '1', 'birth_date' => '1992-11-11', 'city' => 'Київ', 'password' => bcrypt('111111'),
                'telnumber' => '09714414414', 'email' => 'user1@example.com', 'avatar' => '/uploads/users_photos/0/1.jpg',
            ],
            [
                'name' => 'Джанлуїджі', 'sex' => '1', 'birth_date' => '1970-11-11', 'city' => 'Київ', 'password' => bcrypt('111111'),
                'telnumber' => '0984343333', 'email' => 'user2@example.com', 'avatar' => '/uploads/users_photos/1/1.jpg',
            ],
            [
                'name' => 'Джекі', 'sex' => '1', 'birth_date' => '1960-11-11', 'city' => 'Київ', 'password' => bcrypt('111111'),
                'telnumber' => '09714414414', 'email' => 'user3@example.com', 'avatar' => '/uploads/users_photos/2/1.jpg',
            ],
            [
                'name' => 'Анджеліна', 'sex' => '0', 'birth_date' => '1980-11-11', 'city' => 'Київ', 'password' => bcrypt('111111'),
                'telnumber' => '09714414414', 'email' => 'user4@example.com', 'avatar' => '/uploads/users_photos/3/1.jpg',
            ],
            [
                'name' => 'Cандра', 'sex' => '0', 'birth_date' => '1966-11-11', 'city' => 'Київ', 'password' => bcrypt('111111'),
                'telnumber' => '09714414414', 'email' => 'user5@example.com', 'avatar' => '/uploads/users_photos/4/1.jpg',
            ]
        ];

        foreach ($users as $index => $user) {
            $user = User::create($user);
            for ($i = 2; $i <= 4; $i++) {
                Photo::create([
                    'source' => "/uploads/users_photos/{$index}/$i.jpg",
                    'description' => ' ',
                    'user_id' => $user->id
                ]);
            }
        }
    }
}
