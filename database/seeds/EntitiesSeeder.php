<?php

use Illuminate\Database\Seeder;

class EntitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entityNames = [];
        for($i = 1; $i < 50; $i++) {
            DB::table('entity')->insert(
                ['name' => "Entity $i"]
            );
        }

    }
}
