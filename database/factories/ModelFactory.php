<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'email' => $faker->unique()->safeEmail,
        'name' => $faker->name,
        'sex' => random_int(0, 1),
        'birth_date' => date($format = 'Y-m-d'),
        'city_id' => 1,
        'password' => bcrypt('test'),
        'telnumber' => $faker->e164PhoneNumber
    ];
});

$factory->define(App\Models\Like::class, function (Faker\Generator $faker) {
    $likedId = random_int(\DB::table('users')->min('id'), \DB::table('users')->max('id'));

    do {
        $likerId = random_int(\DB::table('users')->min('id'), \DB::table('users')->max('id'));
    }   while ($likedId == $likerId);

    return [
        'liked_id' => $likedId,
        'liker_id' => $likerId
    ];
});
