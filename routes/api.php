<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$this->group(['prefix' => 'v1/', 'namespace' => 'Api'], function () {
    $this->post('auth/register', 'AuthController@register');
    $this->post('auth/login', 'AuthController@login');
    $this->group(['middleware' => 'jwt.auth'], function () {

        $this->get('user/{user_id?}', 'ProfileController@get');
        $this->put('user', 'ProfileController@update');
        $this->get('users/search', 'ProfileController@search');

        $this->post('likes/{user}', 'LikesController@setLike');
        $this->delete('likes/{user}', 'LikesController@unsetLike');
        $this->get('likes', 'LikesController@getMyLikes');

        $this->post('photos', 'PhotosController@store');
        $this->get('photos', 'PhotosController@get');
        $this->delete('photos/{photo}', 'PhotosController@delete');
        $this->put('photos/{photo}', 'PhotosController@update');

        $this->post('meetings/send', 'MeetingsController@send');
        $this->post('meetings/change_status/{meeting}', 'MeetingsController@changeStatus');

        $this->post('gifts/send', 'GiftsController@send');

    });
});